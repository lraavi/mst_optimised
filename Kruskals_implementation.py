import union_find_mod
from collections import defaultdict


class kruskal():

    def run_kruskal(self,graph):
        edges = []
        Tree = defaultdict(lambda : [])
        node_names = []

        #extracting edges from the graph
        for node in graph.get_nodes():
            node_names.append(node.name)
            for neighbour in node.neighbours:
                edges.append((node,neighbour[0],neighbour[1]))

        #sorting the edges with respect to the edge weight
        edges.sort(key= lambda x :x[2])

        #forming the components in Union_find data structure
        u = union_find_mod.union_find(node_names)

        # going thh sorted edges and adding the edge to the MST
        # and checking for cycle formation
        for edge in edges:

            #checking for the cycle formation
            if not u.isconnected(edge[0].name,edge[1].name):

                # merging the two components of the union_find data structure
                u.union(edge[0].name,edge[1].name)

                # adding the edge to MST
                Tree[edge[0].name].append((edge[1].name,edge[2]))

        return Tree
