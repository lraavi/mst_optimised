from Prims_implementation import *
import Kruskals_implementation as kruskal
import Boruvkas_implementation as Borukva
from Graph import Graph,Node


def make_graph(file_name):
    graph = Graph()
    file = open(file_name,'r')
    nodes = defaultdict(lambda : Node())
    for line in file:
        temp_node = nodes[line[0]]
        temp_node.name = line[0]
        neighbours_weights = line[1:-1].strip(",").strip("(").strip(")").split(",")

        for neighbour,weight in zip(neighbours_weights[0::2],neighbours_weights[1::2]):

            temp_node.add_neighbour(nodes[neighbour.strip("(")],int(weight.strip(")")))
            nodes[neighbour.strip("(")].add_neighbour(temp_node,int(weight.strip(")")))

    #adding node to graph
    graph.nodes = list(nodes.values())
    file.close()
    return graph


def make_output_representation_of_graph(graph,filename):

    output_file = open("output"+filename+".txt", "w")

    #converting the MST into the required format
    for i in range(len(graph.nodes())):
        node = graph.nodes[i]
        line = node.name+"("
        for edge in node.neighbours():
            line = line + str(edge) + ","
        line  = line.strip(",") + "),/n"

        if i == len(graph.nodes()-2):
            line = line.strip(",/n") + "/n"

        output_file.write(line)

if __name__=='__main__':
    graph = make_graph("input/graph01.txt")

    MST_prims = Prims().run_prims(graph)
    MST_kruskals = kruskal.kruskal().run_kruskal(graph)
    MST_boruvka = Borukva.Boruvka().run_boruvka(graph)
    print(MST_prims)
    print(MST_kruskals)
    print(MST_boruvka)
