import networkx

class Graph():
    def __init__(self):
        self.nodes = []

    def draw_graph(self):
        pass

    def get_nodes(self):
        return self.nodes

    def add_node(self,node):
        self.nodes.append(node)


class Node():
    def __init__(self,name=""):
        self.name = name
        self.neighbours = []
        self.parent = None

    def neighbours(self):
        return self.neighbours

    def set_parent(self,node):
        self.parent = node

    def add_neighbour(self,node,edge_weight):
        self.neighbours.append((node,edge_weight))