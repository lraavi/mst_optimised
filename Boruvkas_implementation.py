from collections import defaultdict
import union_find_mod
from Graph import Node,Graph
import sys

class Boruvka():

    def __init__(self):
        self.MST = defaultdict(lambda:[])
        self.mst_edges = []

    def recursive_boruvka(self, graph,union_find):

        nodes = graph.get_nodes()

        #finding the minimum edge incident onto each vertex
        for node in nodes:
            if len(node.neighbours) != 0:
                min_edge = min(node.neighbours, key =lambda l : l[1])
                node.neighbours.remove(min_edge)
                self.mst_edges.append((node,min_edge[0],min_edge[1]))

                #checking for cycles and adding the edge to MST
                if union_find.find(node) != union_find.find(min_edge[0]):
                    # print(node.name,min_edge[0].name,min_edge[1])
                    if node.name < min_edge[0].name:
                        self.MST[node.name].append((min_edge[0].name,min_edge[1]))
                    else:
                        self.MST[min_edge[0].name].append((node.name,min_edge[1]))
                    union_find.union(node,min_edge[0])

            # print(node.name,min_edge[0].name,min_edge[1])

        #compress the graph and checking if the number of the number of the vertices in
        #compressed graph is greater than one
        components = union_find.get_components()
        number_of_new_nodes = len(components)

        if number_of_new_nodes > 1:
            super_nodes =  {}
            for i in range(number_of_new_nodes):
                super_nodes[components[i].name] = {}

                for j in range(number_of_new_nodes):
                    if i != j:
                        super_nodes[components[i].name][components[j].name] = (None,None,sys.maxsize)

            #cleaning the graph
            for node in nodes:
                for neighbour in node.neighbours:
                    node_component = union_find.find(node)
                    neighbour_component = union_find.find(neighbour[0])
                    if  node_component != neighbour_component:
                        if super_nodes[node_component.name][neighbour_component.name][2] > neighbour[1]:

                            #edge between components
                            edge = super_nodes[node_component.name][neighbour_component.name]

                            #remove the previous minimum weighted edge between components
                            if edge[0] is not None:
                                edge[0].neighbours.remove((edge[1],edge[2]))
                                edge[1].neighbours.remove((edge[0],edge[2]))

                            #update the edge between components
                            super_nodes[node_component.name][neighbour_component.name] = (node,neighbour[0],neighbour[1])
                            super_nodes[neighbour_component.name][node_component.name] = (node,neighbour[0],neighbour[1])

            self.recursive_boruvka(graph,union_find)



    def run_boruvka(self,graph):
        union_find = union_find_mod.union_find(graph.get_nodes())
        self.recursive_boruvka(graph,union_find)
        return self.MST