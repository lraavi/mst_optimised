class union_find():

    def __init__(self,list_of_elements):
        self.components = {}

        for element in list_of_elements:
            self.components[element] = None

    def find(self,element):
        parent_element = self.components[element]
        if parent_element is not  None:
            return self.find(parent_element)
        else:
            return element

    def union(self,element1,element2):
        parent1 = self.find(element1)
        parent2 = self.find(element2)
        if self.components[parent1] is None and self.components[parent2] is None:
            self.components[parent2] = parent1

    def isconnected(self,element1,element2):
        if self.find(element1) == self.find(element2):
            return True
        else:
            return False

    def get_components(self):
        component_parents = []
        for elemnent,parent in self.components.items():
            if parent is None:
                component_parents.append(elemnent)
        return component_parents