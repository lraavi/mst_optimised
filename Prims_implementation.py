import sys
import fibonacci_heap_mod as heap
from collections import defaultdict


class Prims():

    def run_prims(self,graph,start_node=""):
        nodes = graph.get_nodes()
        Tree = defaultdict(lambda : [])

        #priority_queue required structures
        heap_nodes = defaultdict(lambda : None)
        Heap = heap.Fibonacci_heap()

        if start_node== "":
            start_node = nodes[0]

        #initialisation
        for node in nodes:
            if node.name == start_node.name:
                Tree[start_node.name] = []
                heap_nodes[node.name] = Heap.enqueue(node,0)
            else:
                heap_nodes[node.name] = Heap.enqueue(node,sys.maxsize)


        while (len(Heap) != 0):

            min_weight_heap_object = Heap.dequeue_min()
            min_weight_node = min_weight_heap_object.get_value()
            min_weight = min_weight_heap_object.get_priority()

            # print(min_weight_heap_object.get_value().name, min_weight_heap_object.get_priority())

            #removing the node from the heap nodes list
            del heap_nodes[min_weight_node.name]

            #adding the min weight node to MST
            if min_weight_node.parent != None:
                Tree[min_weight_node.parent.name].append((min_weight_node.name,min_weight))

            for neighbour_of_min_weight_node in min_weight_node.neighbours:
                # print(neighbour_of_min_weight_node[0].name,neighbour_of_min_weight_node[1],min_weight_node.name)

                neighbour_of_min_weight_node[0].set_parent(min_weight_node)

                value = heap_nodes[neighbour_of_min_weight_node[0].name]

                if type(value) is heap.Entry:
                    if value.get_priority() > neighbour_of_min_weight_node[1]:

                        entry_object = heap_nodes[neighbour_of_min_weight_node[0].name]

                        entry_object.get_value().set_parent(min_weight_node)

                        #decrease key in the heap
                        Heap.decrease_key(entry_object,neighbour_of_min_weight_node[1])

        return Tree